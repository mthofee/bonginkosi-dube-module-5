import 'package:module5/dbform.dart';
import 'package:page_transition/page_transition.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyCsSuEktLSRLMBgeq6L1ywgqAZcavREI-U",
          authDomain: "module5-ff809.firebaseapp.com",
          projectId: "module5-ff809",
          storageBucket: "module5-ff809.appspot.com",
          messagingSenderId: "437708359258",
          appId: "1:437708359258:web:f5c36855d839426fcf3a63")
          );
  runApp(const Module5());
}

class Module5 extends StatelessWidget {
  const Module5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: AnimatedSplashScreen(
          duration: 1000,
          splash: 'assets/nature-logo.png',
          nextScreen: const dbForm(),
          splashTransition: SplashTransition.slideTransition,
          pageTransitionType: PageTransitionType.bottomToTop),
      theme: FlexThemeData.light(
        scheme: FlexScheme.green,
        appBarOpacity: 0.68,
        appBarElevation: 2.5,
        subThemesData: const FlexSubThemesData(
          blendOnLevel: 35,
          blendOnColors: false,
          defaultRadius: 27.0,
          textButtonRadius: 7.0,
          elevatedButtonRadius: 9.0,
          outlinedButtonRadius: 9.0,
          elevatedButtonSchemeColor: SchemeColor.primaryContainer,
          inputDecoratorSchemeColor: SchemeColor.onSecondaryContainer,
          inputDecoratorRadius: 35.0,
          dialogRadius: 6.0,
          timePickerDialogRadius: 6.0,
          bottomNavigationBarSelectedLabelSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarUnselectedLabelSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarSelectedIconSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarUnselectedIconSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarOpacity: 0.20,
          bottomNavigationBarElevation: 9.5,
          navigationBarOpacity: 0.69,
          navigationRailOpacity: 0.57,
        ),
        visualDensity: FlexColorScheme.comfortablePlatformDensity,
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
