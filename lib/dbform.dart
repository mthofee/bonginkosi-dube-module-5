// ignore_for_file: camel_case_types

import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module5/detailslist.dart';

class dbForm extends StatefulWidget {
  const dbForm({Key? key}) : super(key: key);

  @override
  State<dbForm> createState() => _dbFormState();
}

class _dbFormState extends State<dbForm> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    Future _addSessions() {
      final firstName = nameController.text;
      final surname = surnameController.text;
      final email = emailController.text;

      final ref = FirebaseFirestore.instance.collection('details').doc();

      return ref
          .set({
            'first_name': firstName,
            'surname': surname,
            'email': email,
            'doc_id': ref.id
          })
          .then((value) => {
                nameController.text = "",
                surnameController.text = "",
                emailController.text = "",
              })
          .catchError(
            (onError) => log(onError),
          );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign up section'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: "First name",
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: surnameController,
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: "Last name",
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: "Email Address",
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: (() {
                        _addSessions();
                      }),
                      child: const Text('Submit'),
                    ),
                  ),
                ),
              ],
            ),
            const DetailsList()
          ],
        ),
      ),
    );
  }
}
