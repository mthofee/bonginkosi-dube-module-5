import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DetailsList extends StatefulWidget {
  const DetailsList({Key? key}) : super(key: key);

  @override
  State<DetailsList> createState() => _DetailsListState();
}

class _DetailsListState extends State<DetailsList> {
  // ignore: non_constant_identifier_names
  final Stream<QuerySnapshot> _MyDetails =
      FirebaseFirestore.instance.collection('details').snapshots();

  final TextEditingController _nameFieldController = TextEditingController();
  final TextEditingController _surnameFieldController = TextEditingController();
  final TextEditingController _emailFieldController = TextEditingController();

  get data => null;
  

  void _delete(docId) {
    FirebaseFirestore.instance
        .collection('details')
        .doc(docId)
        .delete()
        .then((value) => print('Deleted'));

    
  }

  void _update(data) {
    var collection = FirebaseFirestore.instance.collection('details');
    _nameFieldController.text = data['first_name'];
    _surnameFieldController.text = data['surname'];
    _emailFieldController.text = data['email'];

    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: const Text('Edit'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    child: TextField(
                      controller: _nameFieldController,
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: ('First name'),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    child: TextField(
                      controller: _surnameFieldController,
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: ('Last name'),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    child: TextField(
                      controller: _emailFieldController,
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: ('Email'),
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: (() {
                      collection.doc(data["doc_id"]).update({
                        'first_name': _nameFieldController.text,
                        'surname': _surnameFieldController.text,
                        'email': _emailFieldController.text
                      });
                      Navigator.pop(context);
                    }),
                    child: const Text('Update'),
                  )
                ],
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _MyDetails,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text(
              'Something went wrong, please refresh and try again.');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                height: (MediaQuery.of(context).size.height),
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  children: snapshot.data!.docs
                      .map((DocumentSnapshot document) {
                        Map<String, dynamic> data =
                            document.data()! as Map<String, dynamic>;
                        return Column(
                          children: [
                            Card(
                              child: Column(
                                children: [
                                  ListTile(
                                    title: Text(data['first_name']),
                                    subtitle: Text(data['surname']),
                                    trailing: Text(data['email']),
                                  ),
                                  ButtonTheme(
                                      child: ButtonBar(
                                    children: [
                                      OutlinedButton.icon(
                                        onPressed: (() {
                                          _update(data);
                                        }),
                                        icon: const Icon(Icons.edit),
                                        label: const Text('Edit'),
                                      ),
                                      OutlinedButton.icon(
                                        onPressed: () {
                                          _delete(data['doc_id']);
                                        },
                                        icon: const Icon(Icons.delete),
                                        label: const Text('Delete'),
                                      ),
                                    ],
                                  ))
                                ],
                              ),
                            )
                          ],
                        );
                      })
                      .toList()
                      .cast(),
                ),
              ))
            ],
          );
        } else {
          return const Text('No data found');
        }
      },
    );
  }
}

